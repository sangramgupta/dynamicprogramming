

def fib(n):

    arrayLen = [0]*(n+2)
    arrayLen[1] = 1
    for i in range(0, n):
        arrayLen[i+1] = arrayLen[i+1] + arrayLen[i]
        arrayLen[i+2] = arrayLen[i+2] + arrayLen[i]
    return arrayLen[n]


if __name__ == '__main__':
    print(fib(6))