import sys
sys.setrecursionlimit(150090)

def howSum(targetSum, numbers, memo={}):
    if targetSum in memo:
        return memo[targetSum]
    if targetSum < 0:
        return None
    if targetSum == 0:
        return []

    for num in numbers:
        remainder = targetSum - num
        remainderResult = howSum(remainder, numbers, memo)
        if remainderResult != None:
            combination = remainderResult + [num]
            memo[targetSum] = combination.copy()
            return memo[targetSum]

    memo[targetSum] = None
    return None


if __name__ == "__main__":
    print(howSum(1000, [ 3, 4, 7, 2, 1, 25],{}))
    print('A')
    print(howSum(300, [7]))