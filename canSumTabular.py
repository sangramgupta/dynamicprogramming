
def canSum(targetSum, numbers):
    array = [False for i in range(0,len(numbers))]
    array[0] = True
    for m in range(0,len(array)):
        if array[m] == True:
            for n in numbers:
                if m+n<len(array):
                    array[m+n] = True

    return array[len(numbers)-1]


if __name__ == '__main__':
    print(canSum(11, [5,3,4,7]))