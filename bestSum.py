
def bestSum(targetSum, numbers, memo={}):
    if targetSum in memo:
        return memo[targetSum]
    if targetSum == 0:
        return []
    if targetSum < 0:
        return None

    shortestCombination = None
    for num in numbers:
        remainder = targetSum - num
        remainderCombination = bestSum(remainder, numbers, memo)

        if remainderCombination is not None:
            combination = remainderCombination + [num]
            if shortestCombination is None or len(combination) < len(shortestCombination):
                shortestCombination = combination.copy()


    if shortestCombination:
        memo[targetSum] = shortestCombination.copy()
    else:
        memo[targetSum] = None

    return shortestCombination


if __name__ == "__main__":
    print(bestSum(100, [ 25, 1, 3, 5]))