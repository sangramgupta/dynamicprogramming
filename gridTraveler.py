def gridTraveler(m,n, memo={}):
    if (str(m) + "," + str(n)) in memo:
        return memo[str(m) + "," + str(n)]
    if (m == 0) or (n == 0):
        return 0
    if (m == 1) and (n == 1):
        return 1
    memo[str(m) + "," + str(n)] = gridTraveler(m-1,n, memo) + gridTraveler(m,n-1, memo)
    memo[str(n) + "," + str(m)] = memo[str(m) + "," + str(n)]
    return memo[str(m) + "," + str(n)]


if __name__ == '__main__':
    print(gridTraveler(15,9))