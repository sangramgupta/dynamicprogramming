import sys
sys.setrecursionlimit(150090)

def howSum(targetSum, numbers):
    array = [None for i in range(0,targetSum+1)]
    array[0] = []
    for index in range(0,targetSum):
        if array[index] is not None:
            for n in numbers:
                if len(array)> index+n:
                    array[index+n] = array[index] + [n]

    return array[targetSum]


if __name__ == "__main__":
    print(howSum(100, [5,3,7,10]))