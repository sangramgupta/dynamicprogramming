

def canConstruct(target, wordBank, memo={}):
    if target in memo:
        return memo[target]
    if target == '':
        return True

    for word in wordBank:
        if target.find(word) == 0:
            suffix = target.replace(word,'',1)
            if canConstruct(suffix, wordBank, memo) == True:
                memo[target] = True
                return True

    memo[target] = False
    return False


if __name__ == '__main__':
    print(canConstruct("abcdef", ["ab","abc","cd","def","abcd"]))