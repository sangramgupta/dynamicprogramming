
def countConstruct(target, wordBank, memo={}):
    if target in memo:
        return memo[target]
    if target == '':
        return 1

    total = 0
    for word in wordBank:
        if target.find(word) == 0:
            suffix = target.replace(word,'')
            numWaysForRest = countConstruct(suffix, wordBank, memo)
            total = total + numWaysForRest

    memo[target] = total
    return total




if __name__ == '__main__':
    print(countConstruct("abcdef", ["ab", "abc", "cd", "def", "abcd", "ef"]))