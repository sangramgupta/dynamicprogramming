def gridTraveler(m,n):

    table = [[0 for i in range(0,m+1)] for i in range(0,n+1)]
    table[1][1] = 1
    #print(table)
    for p in range(0,m+1):
        for q in range(0,n+1):
            current = table[p][q]
            if q+1 <= n: table[p][q+1] += current
            if p+1 <= m: table[p+1][q] += current
    print(table[m][n])

    return ""


if __name__ == '__main__':
    print(gridTraveler(18,18))