class SolutionBU:
    # @param A : list of integers
    # @param B : integer
    # @return a list of list of integers
    def __init__(self):
        self.result = set()

    def combinationSum(self, A, B):
        def combinationSumRec(numbers, target, prefix=[]):
            if not numbers:
                return
            if target < 0:
                return
            if target == 0:
                self.result.add(tuple(prefix))

            combinationSumRec(numbers,target-numbers[0],prefix + [numbers[0]])
            combinationSumRec(numbers[1:],target, prefix)

        A.sort()
        combinationSumRec(A,B)
        result = sorted(map(list, self.result))
        return result


class Solution:
    # @param C : list of integers
    # @param target : integer
    # @return a list of list of integers
    def combinationSum(self, C, target):
        def combinations_sum_(c, t, prefix=[]):
            if not c:
                return

            if t < 0:
                # backtrack to previous levels
                # the lowest number in C > required target sum
                return
            if t == 0:
                # target-sum can achieved with prefix+C[0]
                # add to results, and backtrack to previous levels
                results.add(tuple(prefix))

            combinations_sum_(c, t - c[0], prefix + [c[0]])
            # upon backtrack, reduce candidate-set and try with the same prefix/target sum
            combinations_sum_(c[1:], t, prefix)

        C.sort()
        # A set is used to de-dup entries incase C itself has duplicates
        # for e.g., if x == x',
        # in which case, [x, x', y] and [x', x, y] will be duplicate
        # entries in the results
        results = set()
        combinations_sum_(C, target)
        return sorted(map(list, results))


foo = SolutionBU()
print(foo.combinationSum([1,2,5,10],100))