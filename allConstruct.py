

def allConstruct(target, wordBank, memo={}):

    if target in memo:
        return memo[target]
    if target == '':
        return [[]]

    result = []

    for word in wordBank:
        if target.find(word) == 0:
            suffix = target.replace(word,'',1)
            suffixWays = allConstruct(suffix, wordBank, memo)
            targetWays = suffixWays.copy()
            for way in targetWays:
                way = [word] + way
                result.append(way)

    memo[target] = result
    return result



if __name__ == '__main__':
    print(allConstruct("abcdef", ["ab", "abc", "cd", "def", "abcd", "c"]))